import HTTP from 'http'

// for HEROKU docker image
const PORT = process.argv[2]

const cout = console.log

function requestHandler(request, response) {

    cout(`Request from client: ${request.url}`)
    response.end(request.url)
}

const SERVER = HTTP.createServer(requestHandler)

SERVER.listen(PORT, (err) => {

    if (err) return cout('something bad happened', err)

    cout(`server is listening on ${PORT}`)
})