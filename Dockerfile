# This file is a template, and might need editing before it works on your project.
# This Dockerfile installs a compiled binary into a bare system.
# You must either commit your compiled binary into source control (not recommended)
# or build the binary first as part of a CI/CD pipeline.

FROM node:8

RUN mkdir app
ADD app_compiled.js ./app/server.js

WORKDIR ./app

# Default command which runs on RUN stage
CMD nodejs server.js $PORT